package pdfbox;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.multipdf.PDFMergerUtility;

public class MergePDFs {
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws IOException {
	      File file1 = new File("D:\\PDFbox\\NIELIT Calicut.pdf");       
	      File file2 = new File("D:\\PDFbox\\file2.pdf");    
			
	      //Instantiating PDFMergerUtility class
	      PDFMergerUtility PDFmerger = new PDFMergerUtility();
			
	      //Setting the destination file
	      PDFmerger.setDestinationFileName("D:\\PDFbox\\Merged Pdfs\\merged.pdf");
			
	      //adding the source files
	      PDFmerger.addSource(file1);
	      PDFmerger.addSource(file2);
			
	      //Merging the two documents
	      PDFmerger.mergeDocuments();
	      System.out.println("Documents merged");
	   }

}
