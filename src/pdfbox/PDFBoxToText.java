package pdfbox;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;



public class PDFBoxToText {

	 /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        PDFManager pdfManager = new PDFManager();
        pdfManager.setFilePath("D:\\PDFbox\\Car Body Styles.pdf");
        try {
            String text = pdfManager.toText();
            System.out.println(text);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
